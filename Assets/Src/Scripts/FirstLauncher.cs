﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.UI;

public class FirstLauncher : MonoBehaviour
{
    [SerializeField]
    private Text _textInitialize;
    
    [SerializeField]
    private Text _textCheckCatalogUpdates;
    
    [SerializeField]
    private Text _textCatalogUpdates;
    
    [SerializeField]
    private Text _firstOpText;
    
    [SerializeField]
    private Text _secondOpText;
    
    [SerializeField]
    private Text _textSceneLoading;

    [SerializeField]
    private Text _text;
    private string _message;

    private AsyncOperationHandle<IResourceLocator>? _initializeAsync;

    private AsyncOperationHandle<List<string>>? _checkCatalogUpdates;
    
    private AsyncOperationHandle<List<IResourceLocator>>? _updateCatalogs;

    private AsyncOperationHandle<SceneInstance>? _sceneLoadingOperation;
    
    
    
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        
        _initializeAsync = null;
        _checkCatalogUpdates = null;
        _updateCatalogs = null;
        _sceneLoadingOperation = null;
        
        _message = "so:\n\r";
        CheckContentUpdate();
    }
    
    void CheckContentUpdate()
    {
        _initializeAsync = Addressables.InitializeAsync();
        _initializeAsync.Value.Completed += objects=>
        {
            _checkCatalogUpdates = Addressables.CheckForCatalogUpdates();
            _checkCatalogUpdates.Value.Completed += checkforupdates =>
            {
                if (checkforupdates.Result.Count > 0)
                {
                    _updateCatalogs = Addressables.UpdateCatalogs();
                    _updateCatalogs.Value.Completed += updates =>
                    {
                        Load();
                    };
                }
                else
                {
                    Load();
                }
            };
        };
    }

    void Update()
    {
        if (_initializeAsync.HasValue)
        {
            var asyncOperationHandle = _initializeAsync.Value;
            if (!asyncOperationHandle.IsDone)
            {
                _textInitialize.text = asyncOperationHandle.PercentComplete.ToString();
            }
        }
        
        if (_checkCatalogUpdates.HasValue)
        {
            var checkCatalogUpdates = _checkCatalogUpdates.Value;
            if (!checkCatalogUpdates.IsDone)
            {
                _textCheckCatalogUpdates.text = checkCatalogUpdates.PercentComplete.ToString();
            }
        }
        
        if (_updateCatalogs.HasValue)
        {
            var updateCatalogs = _updateCatalogs.Value;
            if (!updateCatalogs.IsDone)
            {
                _textCatalogUpdates.text = updateCatalogs.PercentComplete.ToString();
            }
        }

        if (_sceneLoadingOperation.HasValue)
        {
            var sceneLoading = _sceneLoadingOperation.Value;
            if (!sceneLoading.IsDone)
            {
                _textSceneLoading.text = sceneLoading.PercentComplete.ToString();
            }
        }

        if (_firstOp.HasValue)
        {
            AsyncOperationHandle firstOp = _firstOp.Value;
            if (!firstOp.IsDone)
            {
                _firstOpText.text = firstOp.PercentComplete.ToString();
            }
        }

        if (_secondOp.HasValue)
        {
            AsyncOperationHandle secondOp = _secondOp.Value;
            if (!secondOp.IsDone)
            {
                _secondOpText.text = secondOp.PercentComplete.ToString();
            }
        }
    }

    private AsyncOperationHandle? _firstOp = null;
    private AsyncOperationHandle? _secondOp = null;
    
    private void Load()
    {
        _firstOp = Addressables.DownloadDependenciesAsync(PrefabsNames.Chapter_1, true);
        _firstOp.Value.Completed += StartGame;
        _secondOp = Addressables.DownloadDependenciesAsync(PrefabsNames.Chapter_2, true);
        _secondOp.Value.Completed += StartGame;
    }

    private void StartGame(AsyncOperationHandle obj)
    {
        if (_firstOp.HasValue && _firstOp.Value.IsDone)
        {
            if (_secondOp.HasValue && _secondOp.Value.IsDone)
            {
                _sceneLoadingOperation = Addressables.LoadSceneAsync(PrefabsNames.SampleScene);
            }
        }
    }

    private void Load1()
    {
//        var asyncOperationHandle = Addressables.GetDownloadSizeAsync(PrefabsNames.SampleScene);
//        var z = asyncOperationHandle.PercentComplete;
//        asyncOperationHandle.Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.SampleScene, obj);
//        };
//        
//        Addressables.GetDownloadSizeAsync(PrefabsNames.RootImages1).Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.RootImages1, obj);
//        };
//        
//        Addressables.GetDownloadSizeAsync(PrefabsNames.Chapter_1).Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.Chapter_1, obj);
//        };
//        
//        Addressables.GetDownloadSizeAsync(PrefabsNames.Ui_1).Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.Ui_1, obj);
//        };
//        
//        Addressables.GetDownloadSizeAsync(PrefabsNames.RootImages2).Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.RootImages2, obj);
//        };
//        
//        Addressables.GetDownloadSizeAsync(PrefabsNames.Chapter_2).Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.Chapter_2, obj);
//        };
//        
//        Addressables.GetDownloadSizeAsync(PrefabsNames.Ui_2).Completed += (AsyncOperationHandle<long> obj) =>
//        {
//            Roman(PrefabsNames.Ui_2, obj);
//        };
        
        _sceneLoadingOperation = Addressables.LoadSceneAsync("SampleScene");
        
    }

    private void Roman(string addressName, AsyncOperationHandle<long> obj)
    {
        var size = obj.Result;
        string message = $"addressName: {addressName} size: {size}";
        _message += message + "\n\r";
        
        _text.text = _message;
    }
}

public static class PrefabsNames
{
    public const string SampleScene = "SampleScene";
    public const string RootImages1 = "RootImages1";
    public const string RootImages2 = "RootImages2";
    
    public const string Chapter_1 = "chapter_1";
    public const string Chapter_2 = "chapter_2";
}
