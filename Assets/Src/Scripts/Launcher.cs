﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class Launcher : MonoBehaviour
{
    [SerializeField] private Canvas _canvas;

    [SerializeField] private Text _text;
    
    private GameObject _rootImagesPrefab;
    private GameObject _instance;

    private AsyncOperationHandle<List<IResourceLocator>>? _updateCatalogHandler;

    void Start()
    {
        _updateCatalogHandler = null;
        Load();
    }


    private void OnLoadDone(AsyncOperationHandle<GameObject> obj)
    {
        _rootImagesPrefab = obj.Result;

        _instance = Instantiate(_rootImagesPrefab, _canvas.transform, false);
    }

    public void Load()
    {
        Addressables.LoadAssetAsync<GameObject>(PrefabsNames.RootImages1).Completed += OnLoadDone;
        Addressables.LoadAssetAsync<GameObject>(PrefabsNames.RootImages2).Completed += OnLoadDone;
    }

}
