Hello, i have an issue with addressables and https.

Unity version: 2019.2.21f1
Addressables version: 1.8.3

1. I put two prefabs and one scene into addressables groups
2. Made addressables build with "play mode script > use existing build"
3. Filled "Custom certificate handler" inside AddressableAssetSettings with simple script which just "return true"
4. Switched profile to "https"
5. Built addressables and loaded it on server
6. Made app build and installed it on my phone, everything loaded correctly.
7. Then i changed some pictures inside prefab "Assets/Src/Prefabs/RootImages1.prefab"
8. Rebuilt addressables (passed existing *.bin file)
9. Inside server data appeared new json, hash and bundle
10. Loaded it on server again
11. Started app and nothing changed, app still used old bundles.

At the same time, if i use "http" profile and pass data through http protocol, everything works correctly, addressables loads renewed bundles and shows renewed prefabs.

Help me to solve this issue please.

Best regards, Roman;

